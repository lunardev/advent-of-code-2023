# Advent of Code 2023 in Python

[Python3](https://python.org) solutions for [Advent of Code 2023](https://adventofcode.com/2023) puzzles.

Note: [**do not commit puzzle inputs**](https://old.reddit.com/r/adventofcode/wiki/faqs/copyright/inputs).

## Solutions
| Day | Title | Stars | Writeup | Leaderboard | Time (ms) |
|---|---|---|---|---|---|
| [01](./src/day_01/) | Trebuchet?! | ⭐⭐ | ✅ | 179 135/134 389 | 0.547/2.248 |
| [02](./src/day_02/) | Cube Conundrum | ⭐⭐ | ❌ | 117 278/112 690 | 0.311/0.327 |
| [03](./src/day_03/) | Gear Ratios | ⭐⭐ | ❌ | 76 740/66 388 | 1.948/1.99 |
| [04](./src/day_04/) | Scratchcards | ⭐⭐ | ❌ | 4 091/71 607 | 0.759/0.795 |
| [05](./src/day_05/) | If You Give A Seed A Fertilizer | ⭐⭐ | ❌ | 9 195/38 966 | 0.277/**>7200000** |
| [06](./src/day_06/) | Wait For It | ⭐⭐ | ❌ | 4 977/4 375 | 0.013/0.001 |
| [07](./src/day_07/) | Camel Cards | --- | ❌ | --- | --- |
| [08](./src/day_08/) | Haunted Wasteland | ⭐⭐ | ❌ | 2 897/4 675 | 1.377/14.103 |

## Project

The [`pipenv`](https://pypi.org/project/pipenv/) package is used to manage Python dependencies:
```shell
$ python3 -m pip install pipenv
$ pipenv sync
$ pipenv run python3 <solution.py>
```

Since input data is unique to each user, the [`advent-of-code-data`](https://pypi.org/project/advent-of-code-data/) package is used to retrieve puzzle data using a session token. Make sure to save your session token to an environment variable or config file.
```shell
$ export AOC_SESSION=cafef00db01dfaceba5eba11deadbeef
$ echo "cafef00db01dfaceba5eba11deadbeef" > ~/.config/aocd/token
```

## Development

Run unit tests:
```shell
$ pipenv run tests
```

Benchmarking done using [AoC TIMER](https://github.com/caderek/aoctimer):
```shell
$ pipenv run benchmark --part=<1|2> <day>
$ aoctimer --day <day> !!
```

## License
This project is licensed under the [MIT license](./LICENSE).
