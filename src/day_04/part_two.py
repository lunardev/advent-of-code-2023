#!/usr/bin/env python3

"""AoC 2023 Day 4 solution (part 2)."""

from aocd.get import get_data
from aocd.post import submit

DAY = 4
YEAR = 2023


def solution(data: str) -> int:
    cards = data.splitlines()
    card_counts = [0] * len(cards)
    for index, card in enumerate(cards):
        card_counts[index] += 1
        a, b = card.split(":")[-1].split(" | ")
        winning_card = a.split()
        own_card = b.split()

        matches = 0
        for number in own_card:
            matches += number in winning_card
        # The realisation that I do not need to make copies was life-changing.
        for i in range(index + 1, matches + index + 1):
            card_counts[i] += card_counts[index]

    return sum(card_counts)


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=2, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
