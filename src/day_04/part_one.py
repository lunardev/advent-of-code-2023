#!/usr/bin/env python3

"""AoC 2023 Day 4 solution (part 1)."""

from aocd.get import get_data
from aocd.post import submit

DAY = 4
YEAR = 2023


def solution(data: str) -> int:
    scores = 0
    for card in data.splitlines():
        values = card.split(":")[-1]
        a, b = values.split(" | ")
        winning = a.split()
        have = b.split()

        matches = 0
        for i in have:
            matches += i in winning

        # Add nothing to score if no matches.
        if not matches:
            continue

        scores += 2 ** (matches - 1)
    return scores


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=1, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
