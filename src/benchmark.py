#!/usr/bin/env python3

"""Benchmark AoC 2023 Solutions."""

import sys
from argparse import ArgumentParser, Namespace
from datetime import datetime
from timeit import timeit
from zoneinfo import ZoneInfo

from aocd.get import get_data


def disable_std_stream() -> None:
    """Block stdout and stderr."""
    sys.stdout = sys.stderr = None


def enable_std_stream() -> None:
    """Reassign stdout and stderr."""
    sys.stdout = STDOUT
    sys.stderr = STDERR


def limit_day(n: str | int, min_: int = 1, max_: int = 25, /) -> int:
    return max(min(int(n), max_), min_)


def get_part(arg: str) -> str:
    arg = arg.lower()
    parts = {"1": "one", "a": "one", "2": "two", "b": "two"}
    return parts.get(arg, arg)


def parse_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "-p",
        "--part",
        dest="part",
        default="two",
        choices=("1", "2", "a", "b", "one", "two"),
        type=get_part,
        help="benchmark part number (default: two)",
    )
    parser.add_argument(
        "-a",
        "--all",
        dest="all",
        default=False,
        action="store_true",
        help="benchmark all Advent of Code solutions (default: false)",
    )
    parser.add_argument(
        "-n",
        "--number",
        dest="number",
        default=100,
        type=lambda x: max(int(x), 0),
        help="amount of rounds to benchmark; low for speed and high for accuracy (default: 100)",
    )
    parser.add_argument(
        "day",
        default=datetime.now(tz=ZoneInfo("America/New_York")).day,
        nargs="?",
        type=limit_day,
        help="day to benchmark (default: current day in EST timezone)",
    )
    return parser.parse_args()


def main(year: int) -> None:
    args = parse_args()
    disable_std_stream()
    result = timeit(
        "solution(data)",
        setup=f"from day_{args.day:02}.part_{args.part} import solution",
        globals={"data": get_data(year=year, day=args.day)},
        number=args.number,
    )
    enable_std_stream()
    average = result / args.number
    print(f"total time: {average * 1000:.5f}ms")


if __name__ == "__main__":
    STDOUT = sys.stdout
    STDERR = sys.stderr
    main(2023)
