#!/usr/bin/env bash

# Generate a new day folder based on the templae.

basedir=$(dirname -- "$(realpath -- "${0}")")
testsdir="${basedir}/tests"
templatedir="${basedir}/template"

# Check CLI argument or use current day.
re='^[0-9]+$'
if ! [[ "${1}" =~ $re ]]
then
    day=$(date +%d)
else
    day=$(printf "%02d" "${1}")
fi

targetdir="${basedir}/day_${day}"
mkdir -p "${targetdir}"

# Move files from template dir to day dir.
for file in "__init__.py" "part_one.py" "part_two.py" "README.md"
do
    cp -nL "${templatedir}/${file}" "${targetdir}/${file}"
    # Replace "00" in files with correct day.
    nozeroes=$(echo "${day}" | sed "s/^0*//")
    sed -i -e "s/00/${nozeroes}/g" "${targetdir}/${file}"
done

# Move template unittest file.
mkdir -p "${testsdir}"
cp -nL "${templatedir}/day_00.py" "${testsdir}/test_day_${day}.py"
sed -i -e "s/00/${day}/g" "${testsdir}/test_day_${day}.py"

echo "Created ${targetdir}"
exit 0
