#!/usr/bin/env python3

"""AoC 2023 Day 8: Part 2 Solution."""

import math
from itertools import cycle

from aocd.models import Puzzle

DAY = 8
YEAR = 2023


def solution(data: str) -> int:
    instructions, nodes = data.split("\n\n")
    # Create hashmap of nodes.
    node_map: dict[str, tuple[str, str]] = {}
    for node in nodes.splitlines():
        name, lr = node.split(" = ")
        left, right = lr.strip("()").split(", ")
        node_map[name] = (left, right)

    active_nodes = [n for n in node_map if n.endswith("A")]
    steps: list[int] = []
    for step, state in enumerate(cycle(instructions), start=1):
        old_len = len(active_nodes)
        # Remove node if it is an end node.
        active_nodes = [
            node
            for i in active_nodes
            if not (node := node_map[i][state == "R"]).endswith("Z")
        ]
        # Add end node step to list.
        if len(active_nodes) != old_len:
            steps.append(step)

        if not active_nodes:
            break

    # Find the lowest common multiple of all end node steps.
    return math.lcm(*steps)


def main() -> None:
    puzzle = Puzzle(year=YEAR, day=DAY)
    data = puzzle.input_data
    answer = solution(data)
    puzzle.answer_b = answer


if __name__ == "__main__":
    main()
