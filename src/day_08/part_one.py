#!/usr/bin/env python3

"""AoC 2023 Day 8: Part 1 Solution."""

from itertools import cycle

from aocd.models import Puzzle

DAY = 8
YEAR = 2023


def solution(data: str) -> int:
    instructions, nodes = data.split("\n\n")
    # Create hashmap of nodes.
    node_map: dict[str, tuple[str, str]] = {}
    for node in nodes.splitlines():
        name, lr = node.split(" = ")
        left, right = lr.strip("()").split(", ")
        node_map[name] = (left, right)

    current_node = "AAA"
    end_node = "ZZZ"
    step = 0
    # Cycle through the nodes until the end node is reached.
    for step, direction in enumerate(cycle(instructions), start=1):
        current_node = node_map[current_node][direction == "R"]
        if current_node == end_node:
            break
    return step


def main() -> None:
    puzzle = Puzzle(year=YEAR, day=DAY)
    data = puzzle.input_data
    answer = solution(data)
    puzzle.answer_a = answer


if __name__ == "__main__":
    main()
