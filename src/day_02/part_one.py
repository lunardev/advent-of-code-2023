#!/usr/bin/env python3

"""AoC 2023 Day 2 solution (part 1)."""

from aocd.get import get_data
from aocd.post import submit

DAY = 2
YEAR = 2023
LIMITS = {"red": 12, "green": 13, "blue": 14}


def solution(data: str) -> int:
    games = data.splitlines()
    total = 0
    for game in games:
        game_valid = True
        name, sets_str = game.split(": ", 1)
        id_ = int(name.split(" ", 1)[-1])
        for set_ in sets_str.split("; "):
            items = set_.split(", ")
            for item in items:
                amount, colour = item.split()
                if int(amount) > LIMITS[colour]:
                    game_valid = False
        if game_valid:
            total += id_
    return total


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=1, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
