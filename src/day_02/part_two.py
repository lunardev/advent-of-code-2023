#!/usr/bin/env python3

"""AoC 2023 Day 2 solution (part 2)."""

from aocd.get import get_data
from aocd.post import submit

DAY = 2
YEAR = 2023


def solution(data: str) -> int:
    # print(data)
    games = data.splitlines()
    total = 0
    for game in games:
        minimum_sets = {"red": 0, "green": 0, "blue": 0}
        sets_str = game.split(":")[-1]
        for set_ in sets_str.split("; "):
            items = set_.split(", ")
            for item in items:
                amount_str, colour = item.split()
                amount = int(amount_str)
                if amount > minimum_sets[colour]:
                    minimum_sets[colour] = amount
        total += (
            minimum_sets["red"] * minimum_sets["green"] * minimum_sets["blue"]
        )
    return total


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=2, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
