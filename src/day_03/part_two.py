#!/usr/bin/env python3

"""AoC 2023 Day 3 solution (part 2)."""

import itertools
import re

from aocd.get import get_data
from aocd.post import submit

DAY = 3
YEAR = 2023


def solution(data: str) -> int:
    indexes = [m.span() for m in re.finditer(r"[\d]+", data)]
    width = data.index("\n")
    gears: dict[int, list[str]] = {}
    for span in indexes:
        number = data[span[0] : span[1]]
        # Create window around number.
        window = (
            max(span[0] - width - 1, 0),
            span[0] - width + span[1] - span[0],
            span[1] + width + span[0] - span[1],
            min(span[1] + width + 1, len(data) - 1),
        )
        # Chain window indices.
        ranges = itertools.chain(
            range(window[0] - 1, window[1]),
            range(span[0] - 1, span[1] + 1),
            range(window[2], window[3] + 1),
        )
        for index in ranges:
            if data[index] != "*":
                # Ignore if not a gear.
                continue
            # Add number to gear index.
            if gears.get(index) is None:
                gears[index] = [number]
            else:
                gears[index].append(number)

    total = 0
    for numbers in gears.values():
        # If gear has two numbers, add gear ratio to total.
        if len(numbers) != 2:
            continue
        x, y = numbers
        total += int(x) * int(y)
    return total


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=2, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
