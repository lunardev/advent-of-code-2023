#!/usr/bin/env python3

"""AoC 2023 Day 3 solution (part 1)."""

import re

from aocd.get import get_data
from aocd.post import submit

DAY = 3
YEAR = 2023


def solution(data: str) -> int:
    indexes = [m.span() for m in re.finditer(r"[\d]+", data)]
    width = data.index("\n")
    total = 0
    for span in indexes:
        # Carve out a window around each number.
        # ....*
        # .744.
        # .....
        window = (
            max(span[0] - width - 1, 0),
            span[0] - width + span[1] - span[0],
            span[1] + width + span[0] - span[1],
            min(span[1] + width + 1, len(data) - 1),
        )
        box = f"""\
{data[window[0] - 1 : window[1]]}
{data[span[0] - 1]}{data[span[1]]}
{data[window[2]:window[3] + 1]}
"""
        valid = not all(i in ".\n" for i in box)
        if valid:
            total += int(data[span[0] : span[1]])
    return total


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=1, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
