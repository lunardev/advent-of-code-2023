#!/usr/bin/env python3

"""AoC 2023 Day 1 solution (part 1)."""

import re

from aocd.get import get_data
from aocd.post import submit

DAY = 1
YEAR = 2023

# Match written representation to numerical values.
NUMBERS = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}
# Positive lookahead assertion used due to overlapping matches (e.g., oneight).
PATTERN = "(?=({0}))".format("|".join((r"\d", *NUMBERS)))


def find_valid(line: str) -> int:
    """Use pattern matching to find all occurences."""
    matches = [m.span(1) for m in re.finditer(PATTERN, line)]
    valid = ""
    for start, end in matches[0], matches[-1]:
        number = line[start:end]
        # Value defaults to digit if not aword.
        valid += NUMBERS.get(number, number)
    return int(valid)


def solution(data: str) -> int:
    """Solve the puzzle by parsing each line and finding the sum of numbers."""
    return sum(find_valid(i) for i in data.splitlines())


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=2, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
