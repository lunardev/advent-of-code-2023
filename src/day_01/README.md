# Day 1: Trebuchet?!

Link: https://adventofcode.com/2023/day/1<br/>
Leaderboard: **179 135/134 389**

## Part 1

In part one, the puzzle presents the task of finding the first and last digit from lines of text to form a two-digit number. If only one number is present in a line, it is used as both digits. The answer is the sum of all values.

This sounds simple enough; by filtering each line with [`str.isdigit`](https://docs.python.org/3/library/stdtypes.html#str.isdigit) in a list comprehension, all numbers can be isolated:

```pycon
>>> line = "so1u7i0n"
>>> matches = [i for i in line if i.isdigit()]
>>> matches
['1', '7', '0']
```

One tricky case here is if only one digit is present in the line. This can be handled by getting index `-1` of the `matches` list. Since `-1` returns the last index of the list, index `0` and index `-1` produce the same value when a list contains only one item.

```pycon
>>> matches = ["1"]
>>> int(matches[0] + matches[-1])
11
```

These values can be totaled using the [`sum`](https://docs.python.org/3/library/functions.html#sum) builtin in (another) list comprehension, and the puzzle is solved!

## Part 2

Part two introduces the concept of digits being represented by words as well: `one`, `two`, `three`, `four`, `five`, `six`, `seven`, `eight`, and `nine`.

This is more difficult, as a digit is no longer represented by a single character.

The first obvious step here is to create a hash map that links the words to their corresponding digits:

```pycon
>>> numbers = {
...     "one": "1", "two": "2", "three": "3",
...     "four": "4", "five": "5", "six": "6",
...     "seven": "7", "eight": "8", "nine": "9",
... }
```

### Regular expressions
From here, a painless solution for finding both words and digits in the input text would be to utilise regular expressions (regex).

Knowing that the `\d` character represents digits 0-9, the following pattern can be constructed to match any digit or number word:
```pycon
>>> pattern = r"\d|one|two|three|four|five|six|seven|eight|nine"
```
To be more efficient, the key values from the `numbers` dictionary can be used, since those strings have already been defined.
```pycon
>>> "|".join((r"\d", *numbers))
'\\d|one|two|three|four|five|six|seven|eight|nine'
```

The Python standard library has a handy module called [`re`](https://docs.python.org/3/library/re.html) that can be used for regex pattern matching. Matches can be found using the [`re.finditer`](https://docs.python.org/3/library/re.html#re.finditer) method, which yields a [`re.Match`](https://docs.python.org/3/library/re.html#re.Match) object. By calling the [`re.Match.span`](https://docs.python.org/3/library/re.html#re.Match.span) method, a `tuple[int, int]` object is returned that contains the start and end index of the match substring. The matched word or digit can be constructed using string slicing.
```pycon
>>> matches = [m.span(0) for m in re.finditer(pattern, line)]
>>> valid = ""
>>> for start, end in matches[0], matches[-1]:
...     number = line[start:end]
...     # Value defaults to digit if not a word.
...     valid += numbers.get(number, number)
```

### The problem
While this implementation makes logical sense, it still fails AoC's full input test. Why? There is one sneaky edge case that the AoC puzzle fails to mention: **overlapping numbers**.

With the introduction of multi-character digits in the form of words, the risk of words overlapping presents itself. For example, `oneight`, `eightwo`, `twone`, and `fiveight` are all expected to yield two digits. Unfortunately, regex by default in Python does not deal with overlapping values and instead consumes the first number before parsing the second: `1ight`, `8wo`, `2ne`, and `5ight`.

After a bit of research, I discovered [positive lookahead](https://www.regular-expressions.info/lookaround.html#lookahead) [assertions](https://docs.python.org/3/library/re.html#index-21) `(?=(...))` that do not consume matches, therefore allowing overlapping patterns.

Only two small changes need to be made to the implementation to support this. First, the lookahead capturing parenthesis need to be wrapped around the pattern.
```pycon
>>> pattern = "(?=({0}))".format(pattern)))
```

Additionally, the `re.Match.span` group must be [changed from `0` to `1`](https://code.whatever.social/questions/43149086/python-3-regex-find-all-overlapping-matches-start-and-end-index-in-a-string#43149115
) (from the now empty match group to the capture group).

```pycon
>>> matches = [m.span(1) for m in re.finditer(pattern, line)]
```

With that edge case out of the way, part two is solved as well!

All things considered, this was an enjoyable introduction to AoC 2023.
