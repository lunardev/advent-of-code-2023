#!/usr/bin/env python3

"""AoC 2023 Day 1 solution (part 1)."""

from aocd.get import get_data
from aocd.post import submit

DAY = 1
YEAR = 2023


def find_valid(line: str) -> int:
    """Search for any digits then add the first and last match."""
    matches = [i for i in line if i.isdigit()]
    valid = matches[0] + matches[-1]
    return int(valid)


def solution(data: str) -> int:
    """Solve the puzzle by parsing each line and finding the sum of numbers."""
    return sum(find_valid(i) for i in data.splitlines())


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=1, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
