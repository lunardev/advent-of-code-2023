#!/usr/bin/env python3

"""AoC 2023 Day 00 solution (part 2)."""

from aocd.get import get_data
from aocd.post import submit

DAY = 00
YEAR = 2023


def solution(data: str) -> None:
    print(data)


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    print(answer)
    # submit(answer, part=2, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
