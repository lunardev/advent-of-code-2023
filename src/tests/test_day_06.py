import pytest
from aocd.get import get_data

from day_06.part_one import DAY, YEAR
from day_06.part_one import solution as part_one_solution
from day_06.part_two import solution as part_two_solution


@pytest.fixture
def puzzle_data() -> str:
    return get_data(day=DAY, year=YEAR)


@pytest.fixture
def example_data() -> str:
    return """\
Time:      7  15   30
Distance:  9  40  200
"""


def test_part_one_example(example_data: str) -> None:
    answer = part_one_solution(example_data)
    assert answer == 288


def test_part_two_example(example_data: str) -> None:
    answer = part_two_solution(example_data)
    assert answer == 71_503


def test_part_one(puzzle_data: str) -> None:
    answer = part_one_solution(puzzle_data)
    assert answer == 211_904


def test_part_two(puzzle_data: str) -> None:
    answer = part_two_solution(puzzle_data)
    assert answer == 43_364_472
