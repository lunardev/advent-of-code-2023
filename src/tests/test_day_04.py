import pytest
from aocd.get import get_data

from day_04.part_one import DAY, YEAR
from day_04.part_one import solution as part_one_solution
from day_04.part_two import solution as part_two_solution


@pytest.fixture
def puzzle_data() -> str:
    return get_data(day=DAY, year=YEAR)


@pytest.fixture
def example_data() -> str:
    return """\
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
"""


def test_part_one_example(example_data: str) -> None:
    answer = part_one_solution(example_data)
    assert answer == 13


def test_part_two_example(example_data: str) -> None:
    answer = part_two_solution(example_data)
    assert answer == 30


def test_part_one(puzzle_data: str) -> None:
    answer = part_one_solution(puzzle_data)
    assert answer == 32_001


def test_part_two(puzzle_data: str) -> None:
    answer = part_two_solution(puzzle_data)
    assert answer == 5_037_841
