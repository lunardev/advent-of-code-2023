import pytest
from aocd.get import get_data

from day_05.part_one import DAY, YEAR
from day_05.part_one import solution as part_one_solution
from day_05.part_two import solution as part_two_solution


@pytest.fixture
def puzzle_data() -> str:
    return get_data(day=DAY, year=YEAR)


@pytest.fixture
def example_data() -> str:
    return """\
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
"""


def test_part_one_example(example_data: str) -> None:
    answer = part_one_solution(example_data)
    assert answer == 35


def test_part_two_example(example_data: str) -> None:
    answer = part_two_solution(example_data)
    assert answer == 46


def test_part_one(puzzle_data: str) -> None:
    answer = part_one_solution(puzzle_data)
    assert answer == 806_029_445
