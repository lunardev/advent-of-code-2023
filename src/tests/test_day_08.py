import pytest
from aocd.get import get_data

from day_08.part_one import DAY, YEAR
from day_08.part_one import solution as part_one_solution
from day_08.part_two import solution as part_two_solution


@pytest.fixture
def puzzle_data() -> str:
    return get_data(day=DAY, year=YEAR)


def test_part_one_example() -> None:
    data = """\
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
"""
    answer = part_one_solution(data)
    assert answer == 2


def test_part_one_example_2() -> None:
    data = """\
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
"""
    answer = part_one_solution(data)
    assert answer == 6


def test_part_two_example() -> None:
    data = """\
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
"""
    answer = part_two_solution(data)
    assert answer == 6


def test_part_one(puzzle_data: str) -> None:
    answer = part_one_solution(puzzle_data)
    assert answer == 18_827


def test_part_two(puzzle_data: str) -> None:
    answer = part_two_solution(puzzle_data)
    assert answer == 20_220_305_520_997
