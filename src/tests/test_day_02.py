import pytest
from aocd.get import get_data

from day_02.part_one import DAY, YEAR
from day_02.part_one import solution as part_one_solution
from day_02.part_two import solution as part_two_solution


@pytest.fixture
def puzzle_data() -> str:
    return get_data(day=DAY, year=YEAR)


@pytest.fixture
def example_data() -> str:
    return """\
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
"""


def test_part_one_example(example_data: str) -> None:
    answer = part_one_solution(example_data)
    assert answer == 8


def test_part_two_example(example_data: str) -> None:
    answer = part_two_solution(example_data)
    assert answer == 2286


def test_part_one(puzzle_data: str) -> None:
    answer = part_one_solution(puzzle_data)
    assert answer == 1853


def test_part_two(puzzle_data: str) -> None:
    answer = part_two_solution(puzzle_data)
    assert answer == 72_706
