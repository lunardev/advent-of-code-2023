import pytest
from aocd.get import get_data

# from day_01.solution import DAY, YEAR, solution
from day_01.part_one import DAY, YEAR
from day_01.part_one import solution as part_one_solution
from day_01.part_two import solution as part_two_solution


@pytest.fixture
def puzzle_data() -> str:
    return get_data(day=DAY, year=YEAR)


def test_part_one_example() -> None:
    data = """\
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
"""
    answer = part_one_solution(data)
    assert answer == 142


def test_part_two_example() -> None:
    data = """\
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
"""
    answer = part_two_solution(data)
    assert answer == 281


def test_part_one(puzzle_data: str) -> None:
    answer = part_one_solution(puzzle_data)
    assert answer == 56_465


def test_part_two(puzzle_data: str) -> None:
    answer = part_two_solution(puzzle_data)
    assert answer == 55_902
