import pytest
from aocd.get import get_data

from day_03.part_one import DAY, YEAR
from day_03.part_one import solution as part_one_solution
from day_03.part_two import solution as part_two_solution


@pytest.fixture
def puzzle_data() -> str:
    return get_data(day=DAY, year=YEAR)


@pytest.fixture
def example_data() -> str:
    return """\
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
"""


def test_part_one_example(example_data: str) -> None:
    answer = part_one_solution(example_data)
    assert answer == 4361


def test_part_two_example(example_data: str) -> None:
    answer = part_two_solution(example_data)
    assert answer == 467_835


def test_part_one(puzzle_data: str) -> None:
    answer = part_one_solution(puzzle_data)
    assert answer == 538_046


def test_part_two(puzzle_data: str) -> None:
    answer = part_two_solution(puzzle_data)
    assert answer == 81_709_807
