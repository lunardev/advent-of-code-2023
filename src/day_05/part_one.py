#!/usr/bin/env python3

"""AoC 2023 Day 5 solution (part 1)."""

from aocd.get import get_data
from aocd.post import submit

DAY = 5
YEAR = 2023


class RangeDict:
    """Replacement for `dict` object that uses less memory"""

    def __init__(self, ranges: list[list[int]]) -> None:
        self.ranges = [
            (range(j, j + k), range(i, i + k)) for i, j, k in ranges
        ]

    def _translate(self, value: int, origin: range, dest: range) -> int:
        left_span = origin.stop - origin.start
        right_span = dest.stop - dest.start
        scaled_value = float(value - origin.start) / float(left_span)
        return int(dest.start + (scaled_value * right_span))

    def __getitem__(self, value: int, /) -> int:
        for origin, dest in self.ranges:
            if value in origin:
                return self._translate(value, origin, dest)
        return value


def solution(data: str) -> int:
    lines = data.splitlines()

    current_map = None
    headings: dict[str, list[list[int]]] = {}

    # Group headings with associated mappings.
    for line in lines[1:]:
        if not line:
            current_map = None
            continue

        if line[0].isalpha():
            current_map = line.split(":")[0]
            continue

        if current_map is not None:
            range_ = [int(i) for i in line.split()]
            if headings.get(current_map) is None:
                headings[current_map] = [range_]
            else:
                headings[current_map].append(range_)

    # Create range dicts that handle range mappings.
    seed_to_soil = RangeDict(headings["seed-to-soil map"])
    soil_to_fertilizer = RangeDict(headings["soil-to-fertilizer map"])
    fertilizer_to_water = RangeDict(headings["fertilizer-to-water map"])
    water_to_light = RangeDict(headings["water-to-light map"])
    light_to_temp = RangeDict(headings["light-to-temperature map"])
    temp_to_humidity = RangeDict(headings["temperature-to-humidity map"])
    humidity_to_location = RangeDict(headings["humidity-to-location map"])

    lowest = None
    seeds = (int(i) for i in lines[0].split(":")[-1].split())
    # Find lowest location for each seed.
    for seed in seeds:
        soil = seed_to_soil[seed]
        fertilizer = soil_to_fertilizer[soil]
        water = fertilizer_to_water[fertilizer]
        light = water_to_light[water]
        temp = light_to_temp[light]
        humidity = temp_to_humidity[temp]
        location = humidity_to_location[humidity]
        if lowest is None:
            lowest = location
        elif location < lowest:
            lowest = location

    return lowest or -1


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=1, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
