#!/usr/bin/env python3

"""AoC 2023 Day 6 solution (part 1)."""

import math
from typing import Iterable

from aocd.get import get_data
from aocd.post import submit

DAY = 6
YEAR = 2023


def parse_input(text: str) -> Iterable[int]:
    for i in text.split(":")[-1].split():
        yield int(i)


def solution(data: str) -> int:
    a, b = data.splitlines()
    times = parse_input(a)
    distances = parse_input(b)
    wins: list[int] = []
    for time, distance in zip(times, distances):
        win = sum(i * (time - i) > distance for i in range(time + 1))
        wins.append(win)
    return math.prod(wins)


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=1, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
