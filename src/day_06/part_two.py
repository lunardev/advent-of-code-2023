#!/usr/bin/env python3

"""AoC 2023 Day 6 solution (part 2)."""

import math

from aocd.get import get_data
from aocd.post import submit

DAY = 6
YEAR = 2023


def parse_input(text: str) -> int:
    return int(text.replace(" ", "").split(":")[-1])


def quadratic_equation(a: int, b: int, c: int, /) -> tuple[float, float]:
    """Basic quadratic formula calculation."""
    discriminant = b**2 - 4 * a * c
    x1 = (-b + math.sqrt(discriminant)) / 2
    x2 = (-b - math.sqrt(discriminant)) / 2
    return (x1, x2)


def solution(data: str) -> int:
    a, b = data.splitlines()
    time = parse_input(a)
    distance = parse_input(b)
    # Find the two roots.
    x1, x2 = quadratic_equation(1, -time, distance)
    # Return the range of the inside of the roots.
    return int(x1) - int(x2)


def main() -> None:
    data = get_data(day=DAY, year=YEAR)
    answer = solution(data)
    submit(answer, part=2, day=DAY, year=YEAR)


if __name__ == "__main__":
    main()
